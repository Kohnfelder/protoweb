# Quanta prototype

## /quanta

Rquires annotation buttons and a special DIV for annotations
in the target page (so basically only works on one localhost page)
but there is a little API and it basically works.
The server stores quanta in a file so it does remember them.
Quanta are either for the page or for a target element (for now only P tags)
which the quanta aligns with.

## sidepanel1 extension

This is a mockup with no function just to try the web extension mechanics.

## Useful commands

Running for development and debugging
    python server.py
    source qapi.test.sh  # With server up does a few API calls with curl

Running in production
    gunicorn -c gunicorn.conf.py --reload --access-logfile - server:app

Convert the favicon
    convert misc/protwebman.png -resize 64x64 static/favicon.ico

## Server host side commands

curl "https://gitlab.com/Kohnfelder/protoweb/-/archive/main/protoweb-main.zip" >/tmp/project.zip
unzip /tmp/project.zip

nohup gunicorn -c gunicorn.conf.py --access-logfile /tmp/access.log server:app

pkill gunicorn

# TO DO LIST

* Reach into pages to get anchor points
https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/scripting/executeScript

* Firefox web extension port
https://extensionworkshop.com/documentation/develop/porting-a-google-chrome-extension/
