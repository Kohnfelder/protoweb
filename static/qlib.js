// qlib: library of quanta helper functions



qlib = (function() {
    function haash(s) {
	for (var i=0, h=9; i<s.length; )
	    h=Math.imul(h^s.charCodeAt(i++),9**9);
	return (h^h>>>9).toString(16).substr(-4);
    }

    function add_ids(root) {
	for (i = 0; i < root.children.length; i++) {
	    elt = root.children[i];
	    if (["P", "H1", "H2", "H3", "H4"].indexOf(elt.tagName) >= 0) {
		if (!elt.id) {  // Assign paragraph number unless already used
		    n = i+1;
		    seqid = String(1000000000 + n).substr(-3);
		    text = elt.innerText;
		    if (text) {
			qid = '_q:' + haash(text);
		    } else {
			qid = '_q:' + seqid;
		    }
		    if (!document.getElementById(qid))
			elt.id = qid;
		    else
			console.log("Duplicate id: "+qid);
		}
	    }
	}
    }
  
    function collect_ids(root) {
	points = [];
	for (i = 0; i < root.children.length; i++) {
	    elt = root.children[i];
	    if (["P", "H1", "H2", "H3", "H4"].indexOf(elt.tagName) >= 0) {
		text = elt.innerText;
		if (!elt.id) {
		    autoId = text;
		    autoId = autoId.replace(/\s+/g, '_');
		    autoId = autoId.replace(/^[a-zA-Z0-9]+/g, '');
		    autoId = autoId.substr(0, 8);
		    if (!document.getElementById(autoId))
			elt.id = autoId;
		}
		if (elt.id) {
		    text = text.trim();
		    if (text.length < 8)
			text = "<"+elt.tagName+">:"+text;
		    else if (text.length > 18)
			text = text.substr(0, 16) + "...";
		    points.push({'id': elt.id, 'text': text});
		}
	    }
	}
	return points;
    }

    function point_selector(points) {
	// Create <select name="ref"> <option value="id">Text excerpt...</option>
	var sel = document.createElement("select");
	sel.setAttribute("name", "ref");
	for (var i = 0; i < points.length; i++) {
	    pt = points[i];
	    id = pt['id'];
	    if (id.length == 0)
		continue;
	    text = pt['text'];
	    var opt = document.createElement("option");
	    opt.setAttribute("value", id);
	    opt.innerText = text;
	    sel.appendChild(opt);
	}
	return sel;
    }

    return {haash: haash,
	    add_ids: add_ids,
	    collect_ids: collect_ids,
	    point_selector: point_selector
	   };
})();
