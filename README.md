# protoweb

Web prototype of something new...

Quanta web server prototype is at /quanta URL.
It requires annotation buttons and a special DIV for annotations
in the target page (so basically only works on one localhost page)
but there is a little API and it basically works.
The server stores quanta in a file so it does remember them.
Quanta are either for the page or for a target element (for now only `<P>`)
which the quanta aligns with.

* Status: prototype

* Author: Loren Kohnfelder

## Installing the quanta web extension

See extensions/quanta/README.md

## Running the server

    gunicorn -c gunicorn.conf.py --reload --access-logfile /tmp/protoweb.log server:app

## Useful commands

    convert static/protowebman.png -resize 64x64 static/favicon.ico

## Notes


---------
