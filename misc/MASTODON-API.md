# Mastodon API notes

Command line API experiments (using @lmk@infosec.exchange)
using `curl` for API invocations, `jq` to clean up the JSON output.

## Public timeline feed

This grabs the most recent 50 timeline toots from infosec.exchange
and then extracts URLs referenced in the body of the posts.

    curl https://infosec.exchange/api/v1/timelines/public?limit=50 >timelines

Select the content lines which are a limited form of HTML, extract the URLs.
In one sample, 28 of 50 had links

    jq -r '.[].content' timelines | sed -n 's/.*<a href="\([^"]*\).*/\1/p' | less


## Users of an instance

Query for users is limited to up to 80 at a time so it takes many API calls.
(There must be some kind of throttling imposed, but I don't know.)
You can query over all federated users, or restrict to local, and other
[options](https://docs.joinmastodon.org/methods/directory/).

    curl https://infosec.exchange/api/v1/directory'?limit=80&order=active&local=true' >directory
    jq -r '.[].acct' directory | less

The account names of other instances have `@`*domain* suffix, local is just the name.
Crawling would enumerate through all these potentially going to other instances.

## User profile and posts

From the same directory entries you have an `id` for each account needed to access it.
Access by account name I did not see, which is odd because the web interface (not API)
is a well known URL and one can scrape the HTML (so it isn't strictly data protection).

I got my own `id` by query for a rarely used hashtag, and one of the posts was mine.
(I did not see a way in the web settings to get the id, it seems to be an API thing.)

    curl https://infosec.exchange/api/v1/timelines/tag/securebydesign >hash-sbd

*NOTE*: all API outputs are best viewed with `jq <FILE | less` or similar for reading.

With the `id` you can get the profile (subject to public access setting).

    curl https://infosec.exchange/api/v1/accounts/111059912819821691 >lmk-acct

And get toots:

    curl https://infosec.exchange/api/v1/accounts/111059912819821691/statuses >lmk-toots

Finally, the URLs that I have linked in toots:

    jq -r '.[].content' lmk-toots | sed -n 's/.*<a href="\([^"]*\).*/\1/p' >lmk-links

However, this includes lots of links to the social accounts for mentions
(of other accounts, hashtags and the like),
which have `class="u-url mention"` and so forth as extra attribute of the `<a>`.
It looks like a little smarter parsing could easily distinguish actual links.

