"""server: Main server app for the web prototype.
The web server runs on gunicorn using Flask which incorporates Jinja2.
This module handles all the routes for the web server. Use only one worker.
"""

__author__ = "Loren Kohnfelder"

import datetime
import flask
from flask.json import jsonify
import os
import qstore

DEBUG_MODE = True
app = flask.Flask(__name__)
app.config['DEBUG'] = DEBUG_MODE
app.config['TESTING'] = DEBUG_MODE
app.config['TEMPLATES_AUTO_RELOAD'] = DEBUG_MODE
server_mode = "DEBUG" if DEBUG_MODE else "PRODUCTION"

storage_path = "/tmp/protoweb.store"
qstore = qstore.Qstore(storage_path)
if DEBUG_MODE:
    q_debug = {"author": "Jo Debug", "note": "The server started up.",
               "type": "opinion", "url": "http://localhost:5555/quanta",
               "when": datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")}
    qstore.add_quantum(q_debug)


@app.route('/')
def index():
    return flask.render_template('index.html', server_mode=server_mode)


@app.route('/sandbox')
def sandbox():
    return flask.render_template('sandbox.html')


@app.route('/sandbox2')
def sandbox2():
    return flask.render_template('sandbox2.html')


@app.route('/sandbox3')
def sandbox3():
    return flask.render_template('sandbox3.html')


@app.route('/sandbox4')
def sandbox4():
    return flask.render_template('sandbox4.html')


@app.route('/quanta')
def quanta():
    return flask.render_template('quanta.html')


@app.route('/quanta_mock')
def quanta_mock():
    return flask.render_template('quanta_mock.html')


@app.route('/quanta_align')
def quanta_align():
    return flask.render_template('quanta_align.html')


@app.route('/test')
def test():
    return flask.render_template('test.html')


@app.route('/api/quantum', methods=['POST', 'OPTIONS'])
def create_quantum():
    """Creates a new quantum."""
    if flask.request.method == "OPTIONS":
        return cors_preflight_headers()
    data = flask.request.json
    timestamp = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S (%Z)")
    quantum = {'author': data['author'],
               'url': data['url'], 'point': data.get('point', ''),
               'note': data['note'], 'type': data['type'], 'when': timestamp}
    qstore.add_quantum(quantum)
    return cors_preflight_headers(jsonify(quantum))


@app.route('/api/quanta', methods=['POST', 'OPTIONS'])
def find_quanta():
    """Gets all quanta for the given URL.
    This is a form post to carry the URL because the slashes mess up routing.
    """
    if flask.request.method == "OPTIONS":
        return cors_preflight_headers()
    data = flask.request.json
    url = data['url']
    print("/api/quanta :: "+url)
    quanta_for_url = qstore.select_quanta(url)
    return cors_preflight_headers(jsonify(quanta_for_url))


@app.route('/api/hosts', methods=['GET'])
def get_quanta_hosts():
    """Gets all quanta target hosts."""
    hosts = qstore.quanta_hosts()
    return jsonify(hosts)


def cors_preflight_headers(response=None):
    if response is None:
        response = flask.make_response()
        response.headers.add("Access-Control-Allow-Origin", "*")
        response.headers.add("Access-Control-Allow-Headers", "*")
        response.headers.add("Access-Control-Allow-Methods", "*")
    else:
        response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def extra_files(templates_dir):
    """Enumerate template files to auto reload upon change while debugging.
    """
    result = []
    if DEBUG_MODE:
        for dirname, dirs, files in os.walk(templates_dir):
            for filename in files:
                filename = os.path.join(dirname, filename)
                if os.path.isfile(filename):
                    result.append(filename)
    return result


if __name__ == '__main__':
    app.run(debug=DEBUG_MODE, extra_files=extra_files('./templates'))
