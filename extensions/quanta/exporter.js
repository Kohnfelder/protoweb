// exporter.js: script injected into active tab to export annotation points

(function() {

    var errors = [];

    function haash(s) {
	for (var i=0, h=9; i<s.length; )
	    h=Math.imul(h^s.charCodeAt(i++),9**9);
	return (h^h>>>9).toString(16).substr(-4);
    }

    function add_ids(root) {
	elts = root.querySelectorAll("p, h1, h2, h3, h4, h5, h6, h7, h8, h9");
	for (i = 0; i < elts.length; i++) {
	    elt = elts[i];
	    if (!elt.id) {  // Assign paragraph number unless already used
		n = i+1;
		seqid = String(1000000000 + n).substr(-3);
		text = elt.innerText;
		if (text) {
		    qid = '_q:' + haash(text);
		} else {
		    qid = '_q:' + seqid;
		}
		if (!document.getElementById(qid))
		    elt.id = qid;
		else
		    errors.push("Duplicate id: "+qid);
	    }
	}
    }

    function collect_ids(root) {
	points = [];
	elts = root.querySelectorAll("p, h1, h2, h3, h4, h5, h6, h7, h8, h9");
	for (i = 0; i < elts.length; i++) {
	    elt = elts[i];
	    if (elt.id) {
		text = elt.innerText.trim();
		if (text.length < 8)
		    text = "<"+elt.tagName+">:"+text;
		else if (text.length > 18)
		    text = text.substr(0, 16) + "...";
		points.push({'id': elt.id, 'text': text,
			     'y': elt.offsetTop});
	    } else
		errors.push("Noid: "+elt.tagName);
	}
	return points;
    }

    add_ids(document.body);
    points = collect_ids(document.body);
    return [points, errors];
})();
