// qlib.js: library of quanta helper functions

qlib = (function() {

    var dbg = function(){};

    function set_debug_output(f) {
	dbg = f;
    }

    function html_quantum(params) {
	// Create an HTML <div class="quantum"> from a json object params.
	// Example: { "author": "John Doe", "note": "This is a new note",
	//   "type": "correction", "url": "example.com", "point": "q_id",
	//   "when": "2023/09/13 13:06:28 (UTC)"}
	const author = params["author"] || "(anon)";
	const note = params["note"] || "---";
	const kind = params["type"] || "(no type)";
	const url = params["url"] || "#";
	const point = params["point"] || "";
	const when = params["when"] || "(pending)";
	const qDiv = document.createElement("div");
	qDiv.setAttribute("class", "quantum");
	var p = document.createElement("p");
	var node = document.createTextNode(note);
	var elt = document.createElement("br");
	p.appendChild(node);
	p.appendChild(elt);
	var showPoint = point && ('@{'+point+'} ');
	node = document.createTextNode(showPoint + "by "+author+" ");
	p.appendChild(node);
	node = document.createTextNode("["+kind+"]");
	elt = document.createElement("i");
	elt.appendChild(node);
	p.appendChild(elt);
	elt = document.createElement("br");
	p.appendChild(elt);
	node = document.createTextNode(" at ");
	p.appendChild(node);
	node = document.createTextNode(when);
	elt = document.createElement("i");
	elt.appendChild(node);
	p.appendChild(elt);
	qDiv.appendChild(p);
	if (point.length > 0)
	    qDiv.q_point = point;
	return qDiv;
    }

    function point_selector(sel, points) {
	// The first option of {sel} <select> is for the whole page. Remove rest.
	opts = sel.children;
	for (var i = opts.length; i > 1; i--)
	    sel.removeChild(opts[i-1]);
	// From {points} insert <option value="id">Text excerpt...</option> elts.
	for (var i = 0; i < points.length; i++) {
	    pt = points[i];
	    id = pt['id'];
	    if (id.length == 0)
		continue;
	    text = pt['text'];
	    var opt = document.createElement("option");
	    opt.setAttribute("value", id);
	    opt.innerText = text;
	    sel.appendChild(opt);
	}
	sel.children[0].selected = true;  // Preselect whole page
	return sel;
    }

    function newQuanta(cb) {
	// create a new quantum on the quanta server from form fields
	// on completion callback async to cb(success) with true/false
	dbg("createQ...");
	pf = document.getElementById('addQuantum');
	req = {"author": "(anonymous)", "url": "example.com",
	       "point": "", "note": "---", "type": "annotation"};
	fields = pf.children;
	for (i = 0; i < fields.length; i++) {
	    field = fields[i].name
	    if (field in req) {
		entry = fields[i].value;
		if (entry)
		    req[field] = entry;
	    }
	}
	var xhr = new XMLHttpRequest();
	var url = server_url('/api/quantum');
	dbg("createQ: " + url);
	xhr.open("POST", url);
	xhr.setRequestHeader("Content-Type", "application/json");
	var data = JSON.stringify(req);
	xhr.onload = function() {
	    if (xhr.status === 200) {
		var response = JSON.parse(xhr.responseText);
		var elt = html_quantum(response);
		// Local prepend
		const targetDiv = document.getElementById("quanta-annotations");
		var qDiv = html_quantum(req);
		targetDiv.appendChild(qDiv);
		qDiv.scrollIntoView();
		dbg("createQ(" + url + ") | "+xhr.responseText);
		cb(true);
	    } else {
		dbg("Error status creating quantum: " + xhr.status + "<==" + data);
		cb(false);
	    }
	};
	xhr.onerror = function() {
	    dbg("Error creating quantum: " + url);
	    cb(false);
	};
	xhr.send(data);
	return false;
    }

    function populateQuanta(qList) {
	const targetDiv = document.getElementById("quanta-annotations");
	targetDiv.innerHTML = ""; // clear out previous quanta if any
	if (qList.length == 0) {
	    if (qlib.glob.p_none)
		targetDiv.appendChild(qlib.glob.p_none);
	    return;
	}
	for (iq = 0; iq < qList.length; iq++) {
	    q = qList[iq];
	    var qelt = html_quantum(q);
	    var point = q.point;
	    if (point)
		qelt.addEventListener("click", (event) => {
		    dbg("clicked " + point);
		    pointSelected(point);  // TODO: back into quanta-panel.js
		});
	    targetDiv.appendChild(qelt);
	    qelt.style.display = "block";
	}
    }

    function queryQuanta(req) {
	// query the Quanta server with a request to get quanta for the page
	var xhr = new XMLHttpRequest();
	var url = server_url('/api/quanta');
	xhr.open("POST", url);
	xhr.setRequestHeader("Content-Type", "application/json");
	var data = JSON.stringify(req);
	xhr.send(data);
	xhr.onload = function() {
	    if (xhr.status === 200) {
		var response = JSON.parse(xhr.responseText);
		if (response) {
		    populateQuanta(response);
		}
	    } else {
		dbg("Error: " + xhr.status + "<==" + data);
	    }
	};
	xhr.onerror = function() {
	    dbg("Error querying quanta: " + url);
	};
	return false;
    }

    function loadQuanta(url) {
	// active tab change or initial setup when sidebar opens
	var urlField = document.getElementById("addQurl");
	var createButton = document.getElementById("addQcreate");
	if (info.url.startsWith('http')) {
	    if (createButton)
		createButton.disabled = false;
	    req = {"url": url};
	    queryQuanta(req);
	    if (urlField)
		urlField.value = url;
	} else {
	    if (urlField)
		urlField.value = "";
	    if (createButton)
		createButton.disabled = true;
	}
    }

    function set_quanta_host(urlbase) {
	qlib.glob.host = urlbase;
    }

    function server_url(path) {
	host = qlib.glob.host;
	url = host + path;
	return url;
    }

    function haash(s) {
	for (var i=0, h=9; i<s.length; )
	    h=Math.imul(h^s.charCodeAt(i++),9**9);
	return (h^h>>>9).toString(16).substr(-4);
    }

    function add_ids(root) {
	for (i = 0; i < root.children.length; i++) {
	    elt = root.children[i];
	    if (["P", "H1", "H2", "H3", "H4"].indexOf(elt.tagName) >= 0) {
		if (!elt.id) {  // Assign paragraph number unless already used
		    n = i+1;
		    seqid = String(1000000000 + n).substr(-3);
		    text = elt.innerText;
		    if (text) {
			qid = '_q:' + haash(text);
		    } else {
			qid = '_q:' + seqid;
		    }
		    if (!document.getElementById(qid))
			elt.id = qid;
		    else
			console.log("Duplicate id: "+qid);
		}
	    }
	}
    }
  
    function collect_ids(root) {
	points = [];
	for (i = 0; i < root.children.length; i++) {
	    elt = root.children[i];
	    if (["P", "H1", "H2", "H3", "H4"].indexOf(elt.tagName) >= 0) {
		text = elt.innerText;
		if (!elt.id) {
		    autoId = text;
		    autoId = autoId.replace(/\s+/g, '_');
		    autoId = autoId.replace(/^[a-zA-Z0-9]+/g, '');
		    autoId = autoId.substr(0, 8);
		    if (!document.getElementById(autoId))
			elt.id = autoId;
		}
		if (elt.id) {
		    text = text.trim();
		    if (text.length < 8)
			text = "<"+elt.tagName+">:"+text;
		    else if (text.length > 18)
			text = text.substr(0, 16) + "...";
		    points.push({'id': elt.id, 'text': text});
		}
	    }
	}
	return points;
    }

    function anchor_annotations(qanno) {
	// qanno is DIV for annotations; remove <P>(none)</P> saved for later.
	qlib.glob.qanno = qanno;
	var elt = qanno.removeChild(qlib.glob.qanno.children[0]);
	qlib.glob.p_none = elt;
    }

    return {
	glob: {},
	set_debug_output: set_debug_output,
	set_quanta_host: set_quanta_host,
	html_quantum: html_quantum,
	newQuanta: newQuanta,
	queryQuanta: queryQuanta,
	loadQuanta: loadQuanta,
	haash: haash,
	point_selector: point_selector,
	anchor_annotations: anchor_annotations
    };
})();
