# Quanta panel prototype

This web extension prototype is an early Quanta service for web page annotation.

## Running this extension

0. Enable Chrome Developer Mode.
1. Clone or download this repository.
2. Load the `extensions/quanta` directory in Chrome as an [unpacked extension](https://developer.chrome.com/docs/extensions/mv3/getstarted/development-basics/#load-unpacked).
3. Open the side panel UI.
4. Choose the "Quanta prototype".

## Notes on permission

We need broad host access (all sites) to script the active tab.
Just "activeTab" permission is not sufficient in Chrome to inject script.
