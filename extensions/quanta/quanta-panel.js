// Script for the quanta-panel.html sidebar (depends on jlib.js)

currentTabId = -1;

function dbg(str) { // Debug support
    console.log(str);
    var textbox = document.getElementById("dbg-textbox");
    if (textbox)
	textbox.innerText = str;
}


function pointSelectedInTab(id) {
    // This function is serialized and run in the active tab context
    elt = document.getElementById(id);
    if (elt)
	elt.scrollIntoView();
}

function pointSelected(id) {
    if (currentTabId > 0)
	chrome.scripting.executeScript({
	    args: [id],
	    func: pointSelectedInTab,
	    target: {tabId: currentTabId}
	});
}

function cbError(reason) {
    dbg(reason);
    currentTabId = -1;
}

function cbExecuted(result) {
    // result=[{... "result":[[{"id":"_q:52f2","text":"...","y":0},...],[]]}]
    dbg("result="+JSON.stringify(result));
    result = result[0].result;
    points = result[0];
    dbg("Imported " + points.length + ": " + JSON.stringify(result[1]));
    sel = document.getElementById('anno-point');
    if (sel)
	qlib.point_selector(sel, points);
}

function importFromTab(tabId) {
    // Chrome only for now.
    // Firefox? ==> browser.scripting.executeScript({file: '/exporter.js'})
    chrome.scripting.executeScript({
	target : {tabId : tabId, allFrames : false},
	files: ['/exporter.js']
    }).then(cbExecuted, cbError);
}

function cbActiveTab(tabsArray) {
    // on sidebar load only: find the currently active tab
    if (tabsArray.length==1)
	cbTabInfo(tabsArray[0]);
    else
	dbg("Got active tab query count: " + tabsArray.length);
}

function cbTabInfo(tabInfo) {
    // switch to active tab (or on sidebar open set it up initially)
    if (!tabInfo) {
	dbg("No tabInfo!!");
	return;
    }
    currentTabId = tabInfo.id;
    sel = document.getElementById('anno-point');
    if (sel)
	qlib.point_selector(sel, []); // Clear selector early
    importFromTab(tabInfo.id);
    info = {height: tabInfo.height,
	    id: tabInfo.id,
	    incognito: tabInfo.incognito,
	    pinned: tabInfo.pinned,
	    title: tabInfo.title,
	    url: tabInfo.url,
	    width: tabInfo.width};
    sidebar_reload();
}

function tabActivated(activeInfo) {
    currentTabId = activeInfo.tabId;
    chrome.tabs.get(activeInfo.tabId).then(cbTabInfo, cbError);
}

function tabsUpdated(tabId, changeInfo, tabInfo) {
    if (currentTabId < 0) {
	currentTabId = tabId;
	sidebar_reload();
    }
    else if (currentTabId == tabInfo.id)
	if (changeInfo.url)
	    cbTabInfo(tabInfo);
}

function createQuantum() {
    qlib.newQuanta(
	function(success) {
	    dbg("newQuanta=" + success);
	    sidebar_reload();
	});
}

function sidebar_reload() {
    if (currentTabId > 0)
	qlib.loadQuanta(info.url);
    else {
	dbg("sidebar_reload: no current tab");
	qlib.loadQuanta("");
    }
}

function set_host() {
    const selector = document.getElementById('host-selector');
    const host = selector.value;
    qlib.set_quanta_host(host);
    dbg("Host: " + host);
    sidebar_reload();
}

function loaded() {
    qlib.set_debug_output(dbg);
    const selector = document.getElementById('host-selector');
    const host = selector.value;
    qlib.set_quanta_host(host);
    selector.addEventListener('change', set_host);
    chrome.tabs.onActivated.addListener(tabActivated);
    chrome.tabs.onUpdated.addListener(tabsUpdated);
    var qanno = document.getElementById("quanta-annotations");
    if (qanno) {
	qlib.anchor_annotations(qanno);
    } else
	dbg("qanno missing!");
    chrome.tabs.query({active: true}).then(cbActiveTab, cbError);
    var createButton = document.getElementById("addQcreate");
    createButton.addEventListener("click", createQuantum);
    var pointSel = document.getElementById("anno-point");
    pointSel.addEventListener("change", function() {
	var id = pointSel.value;
	pointSelected(id);
    });
}

document.addEventListener("DOMContentLoaded", loaded);
