#!/bin/bash

echo Create a new note
curl -X POST -H "Content-Type: application/json" -d '{
  "author": "John Doe",
  "url": "https://example.com",
  "note": "This is a new note",
  "type": "correction"
}' http://localhost:5657/api/quantum

echo Get all notes
curl -X POST -H "Content-Type: application/json" -d '{
  "url": "*"
}' http://localhost:5657/api/quanta

echo Get notes for the URL "example.com"
curl -X POST -H "Content-Type: application/json" -d '{
  "url": "https://example.com"
}' http://localhost:5657/api/quanta

echo Get hosts
curl http://localhost:5657/api/hosts
