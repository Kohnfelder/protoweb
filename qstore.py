"""qstore: Storage for quanta with query access functions.
Quanta are stored in memory on the server, and each new quantum
is appended to a file as a persistent record. (Delete the file to empty it.)
When the server starts up it loads previous quanta from the file to restore.
"""


__author__ = "Loren Kohnfelder"


import json
import os
from urllib.parse import urlparse


class Qstore:
    """State manager for the prototype Quanta web server.
    """
    def __init__(self, path=None):
        """Create a new instance of storage, done once at server startup.
        If *path* is specified the file is opened and read to initialize
        state to previous value. As additional content is added
        these are appended to the file as persistent storage.
        >>> test_path = '/tmp/test_qstore'
        >>> try:
        ...     os.remove(test_path)
        ... except OSError:
        ...     pass
        >>> qs = Qstore(test_path)
        >>> q1 = {'seq': 1, 'note': 'Test1', 'url': 'https://example.com'}
        >>> qs.add_quantum(q1)
        >>> all = qs.all_quanta()
        >>> len(all)
        1
        >>> q2 = {'seq': 2, 'note': 'Test2', 'url': 'https://example.com'}
        >>> q3 = {'seq': 3, 'note': 'Test3', 'url': 'http://localhost'}
        >>> qs.add_quantum(q2)
        >>> qs.add_quantum(q3)
        >>> all = qs.all_quanta()
        >>> len(all)
        3
        >>> qs2 = Qstore(test_path)
        >>> all = qs2.all_quanta()
        >>> len(all)
        3
        """
        self.quanta = []
        self.backing_path = path
        if path:
            if os.path.isfile(path):
                with open(path, "r") as f:
                    for line in f:
                        entry = json.loads(line)
                        self.quanta.append(entry)

    def add_quantum(self, quantum):
        """Creates a new quantum quantum.
        >>> qs = Qstore()
        >>> edc = 'https://example.com'
        >>> q1 = {'seq': 1, 'note': 'Test1', 'url': edc}
        >>> q2 = {'seq': 2, 'note': 'Test2', 'url': edc, 'point': 'testid'}
        >>> q3 = {'seq': 3, 'note': 'Test3', 'url': 'http://localhost'}
        >>> qs.add_quantum(q1)
        >>> qs.add_quantum(q2)
        >>> qs.add_quantum(q3)
        >>> all = qs.all_quanta()
        >>> cum = set()
        >>> for q in all:
        ...     if q['seq'] & 1:
        ...         cum.add(q['note'])  # set of odd seq notes
        ...     else:
        ...         cum.add(q['point']) # point id for evens
        >>> '|'.join(sorted(cum))
        'Test1|Test3|testid'
        """
        self.quanta.append(quantum)
        s = json.dumps(quantum)
        if self.backing_path:
            with open(self.backing_path, "a+") as f:
                f.write(s + "\n")

    def select_quanta(self, url):
        """Select quanta for a given url (* for all); returns list of quantum.
        >>> qs = Qstore()
        >>> q1 = {'seq': 1, 'note': 'Test1', 'url': 'https://example.com'}
        >>> q2 = {'seq': 2, 'note': 'Test2', 'url': 'https://example.com'}
        >>> q3 = {'seq': 3, 'note': 'Test3', 'url': 'http://localhost'}
        >>> qs.add_quantum(q1)
        >>> qs.add_quantum(q2)
        >>> qs.add_quantum(q3)
        >>> excom = qs.select_quanta('https://example.com')
        >>> [q['note'] for q in excom]
        ['Test1', 'Test2']
        >>> lh = qs.select_quanta('http://localhost')
        >>> len(lh)
        1
        >>> lh[0]['seq']
        3
        >>> nosuch = qs.select_quanta('goofy')
        >>> len(nosuch)
        0
        >>> q4 = {'seq': 4, 'note': 'Test4', 'url': 'http://localhost#target'}
        >>> qs.add_quantum(q4)
        >>> lh2 = qs.select_quanta('http://localhost#target')
        >>> len(lh2)
        1
        >>> lh2[0]['seq']
        4
        >>> lh3 = qs.select_quanta('http://localhost')
        >>> len(lh3)
        2
        >>> int(lh3[0]['seq'])+int(lh3[1]['seq'])
        7
        """
        if url == '*':
            the_quanta = self.quanta
        else:
            the_quanta = [q for q in self.quanta if q['url'].startswith(url)]
        return the_quanta

    def quanta_hosts(self):
        """Returns a list of all domain hosts for quanta the server has stored.
        >>> qs = Qstore()
        >>> q1 = {'seq': 1, 'note': 'Test1', 'url': 'https://example.com'}
        >>> q2 = {'seq': 2, 'note': 'Test2', 'url': 'https://example.com'}
        >>> q3 = {'seq': 3, 'note': 'Test3', 'url': 'http://localhost'}
        >>> qs.add_quantum(q1)
        >>> qs.add_quantum(q2)
        >>> qs.add_quantum(q3)
        >>> qs.quanta_hosts()
        ['example.com', 'localhost']
        """
        hosts = set()
        for quantum in self.quanta:
            url = quantum['url']
            if url:
                urlo = urlparse(url)
                host = None
                if urlo.scheme:
                    host = urlo.hostname
                else:
                    host = urlo.path
                if host:
                    hosts.add(host)
        return sorted(hosts)

    def quanta_host_urls(self, hostname):
        """Returns a list of urls for quanta of a host the server has stored.
        >>> qs = Qstore()
        >>> q1 = {'seq': 1, 'note': 'Test1', 'url': 'https://example.com'}
        >>> q2 = {'seq': 2, 'note': 'Test2', 'url': 'https://example.com/t2'}
        >>> q3 = {'seq': 3, 'note': 'Test3', 'url': 'http://localhost'}
        >>> qs.add_quantum(q1)
        >>> qs.add_quantum(q2)
        >>> qs.add_quantum(q3)
        >>> qs.quanta_host_urls('example.com')
        ['/', '/t2']
        """
        urls = set()
        for quantum in self.quanta:
            url = quantum['url']
            if url:
                urlo = urlparse(url)
                host = None
                if urlo.scheme:
                    host = urlo.hostname
                    path = urlo.path
                    if len(path) == 0:
                        path = '/'
                    if host == hostname:
                        urls.add(path)
        return sorted(urls)

    def all_quanta(self):
        """Returns a list of all quanta the server has stored.
        """
        return self.quanta


if __name__ == "__main__":
    import doctest
    if doctest.testmod().failed:
        print("Tests FAIL")
